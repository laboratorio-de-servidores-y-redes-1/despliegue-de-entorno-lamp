# Trabajo práctico: Despliegue entorno LAMP



## Instrucciones

> Esto es lo que se solicitó en el TP

Desplegar Wordpress en un servidor web Apache2 con las siguientes características:

 - Dentro del raíz del sitio debe existir un directorio protegido con contraseña.
 - El sitio debe ser accedido mediante el puerto 8080.
 - El sitio solo puede ser accedido desde la red local.
 - Los permisos de lectura y escritura del directorio raíz del sitio debe estar restringido a los procesos de Apache2 que correspondan.
 - Agregar zona y registros dns.



Describir y explicar los pasos de instalación del stack LAMP.
Describir y explicar los pasos de permisos sobre directorios.



## Introducción

> Interpretación de las instrucciones, sienta las bases del desarrollo del proyecto.

Según las instrucciones se procederá a instalar y configurar un entorno LAMP en el cual se desplegara la aplicación Wordpress con determinadas características:

- Dentro del raíz de la aplicación (DocumentRoot del VirtualHost) debe haber un directorio protegido con contraseña.
- El puerto de escucha del servicio es el 8080.
- Se restringe todas aquellas solicitudes que provengan fuera de la red local.
- Ajustar los permisos del directorio raíz del VirtualHost (aplicación) acorde a dueño y grupo de los procesos de Apache2 que atienden solicitudes.
- Agregar la zona con sus correspondientes registros para acceder al servicio.



> En este punto debemos planificar como llevar adelante el proyecto, lo mejor es dividirlo en etapas encadenadas.



El proyecto se realizara en las siguientes etapas.

**Etapa 1: DNS**

Antes de desplegar la aplicación y cualquier otra cosa lo primero que necesitamos es un nombre de dominio, ya que es mediante este que podemos acceder al servicio ya sea en etapas de prueba como puesta en producción.

En esta etapa se crea la zona y configura SOA.

**Etapa 2: Entorno LAMP**

Antes de definir el VirtualHost correspondiente a la aplicación, debemos tener preparado el entorno de ejecución para la aplicación. En esta etapa se realiza la instalación y configuración del stack LAMP.

**Etapa 3: VirtualHost**

Definir el VirtualHost para la aplicación según las características señaladas anteriormente. En esta etapa se definen las directivas sobre autenticación y acceso, nombre del servicio, directorio raíz, directorio protegido.

**Etapa 4: Despliegue de Wordpress**

Cuando todo el entorno esta listo para contener una aplicación se procede al despliegue de la misma y por ultimo la verificación de permisos de directorios y archivos.

## Entorno/Requisitos

Para el desarrollo del proyecto se necesita el siguiente entorno:

- Servidor con sistema operativo Debian 12 con dirección IP `192.168.11.200/24`.

- Servidor dns BIND9 con dirección IP `192.168.11.210/24`.

> Por las máscaras de red inferimos que ambos hosts se encuentran en la misma red.



## Desarrollo

### Etapa 1: DNS

#### Configuración

En esta primera etapa, configuramos nuestro servidor DNS para manejar el dominio `wordpress.tp`. Esto nos permite resolver este nombre de dominio dentro de nuestra red local.

**Declaración de zona**

Primero, en el archivo `named.conf.local`, definimos la zona `wordpress.tp` como una zona maestra y especificamos el archivo donde se encuentran los registros DNS para este dominio.

Añadir la zona en el archivo `/etc/bind/named.conf.local`.

```
zone "wordpress.tp" {    
    type master;    
    file "/etc/bind/db.wordpress.tp"; 
    };
```

**Configuración SOA**

Luego, creamos el archivo `db.wordpress.tp` que contiene los registros DNS. Establecemos la autoridad principal para el dominio, asignamos direcciones IP a los nombres `wordpress.tp` y `ns.wordpress.tp`, y creamos un alias para que `www.wordpress.tp` apunte a `wordpress.tp`.

Declaración de inicio de zona de autoridad y registros dns. 

Fichero `/etc/bind/db.wordpress.tp`.

```
$TTL 1D
$ORIGIN wordpress.tp.

@ IN SOA ns.wordpress.tp. admin.wordpress.tp. (

	1	; Serial
	8H	; Refresh
	2H	; Retry
	4W	; Expire
	1D	; Minum TTL
)
; Registros ns
@		IN	NS	ns.wordpress.tp.
ns		IN	A		192.168.11.210

; El dominio 'wordpress.tp' debe apuntar a la dirección ip del servidor
; que entrega el recurso solicitado.
@		IN	A		192.168.11.200

; Alias
www		IN	CNAME	wordpress.tp
```

Estos pasos configuran nuestro servidor DNS para que pueda resolver las direcciones dentro de nuestra red. Esto es esencial para que Apache2 capture la petición para un Virtual Host específico, según el campo Host del mensaje HTTP.

#### Prueba

En un sistema operativo que tenga configurado como servidor dns primario el servidor `192.168.11.210`.

```
$ host wordpress.tp
wordpress.tp has address 192.168.11.200
```



### Etapa 2: Entorno LAMP

En esta etapa, instalamos y configuramos el entorno LAMP necesario para ejecutar la aplicación WordPress. Utilizamos los siguientes comandos para instalar Apache2, MariaDB y PHP.

#### Instalación

Actualización de base de datos de paquetes.

```
# apt update
```

Instalación de paquetes.

```
# apt install apache2 mariadb-server php libapache2-mod-php php-mysql
```

Estos comandos instalan el servidor web Apache2, el servidor de bases de datos MariaDB y PHP junto con los módulos necesarios para que Apache2 y MariaDB trabajen con PHP. Esta configuración es crucial para proporcionar un entorno de ejecución adecuado para la aplicación WordPress.

#### Prueba

Actualmente el dominio `wordpress.tp` apunta a la dirección IP `192.168.11.200`, actualmente no hay un VirtualHost configurado pero ante una petición HTTP debería de responder con el default host.

Se usa el comando `curl` en un sistema operativo que tenga configurado como servidor dns primario el servidor `192.168.11.210`.

```
$ curl -v -s wordpress.tp
*   Trying 192.168.11.200:80...
* Connected to wordpress.tp (192.168.11.200) port 80 (#0)
> GET / HTTP/1.1
> Host: wordpress.tp
> User-Agent: curl/7.81.0
> Accept: */*
> 
* Mark bundle as not supporting multiuse
< HTTP/1.1 200 OK
< Date: Wed, 26 Jun 2024 18:27:42 GMT
< Server: Apache/2.4.59 (Debian)
< Last-Modified: Wed, 26 Jun 2024 18:25:08 GMT
< ETag: "29cd-61bcf22552b59"
< Accept-Ranges: bytes
< Content-Length: 10701
< Vary: Accept-Encoding
< Content-Type: text/html
< 
<html xmlns="http://www.w3.org/1999/xhtml">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Apache2 Debian Default Page: It works</title>
    ...
</html>
```



### Etapa 3: VirtualHost

#### Creación de VirtualHost

En esta etapa, configuramos un VirtualHost en Apache para nuestro sitio WordPress. Creamos un archivo de configuración en `/etc/apache2/sites-available/wordpress.tp.conf` para que Apache escuche en el puerto 8080. Establecemos el nombre del servidor como `wordpress.tp` y opcionalmente agregamos `www.wordpress.tp` como alias. Definimos el directorio raíz de la aplicación en `/var/www/wordpress`.

Restringimos el acceso al sitio web solo a la subred `192.168.11.0/24` para asegurar que solo dispositivos dentro de esta red puedan acceder al sitio. Además, protegemos con contraseña el directorio `/var/www/wordpress/protegido`, configurando la autenticación básica para que solo los usuarios autorizados puedan acceder.

Fichero `/etc/apache2/sites-available/wordpress.tp.conf`.

```
# Puerto de escucha 8080
<VirtualHost *:8080>
    ServerAdmin webmaster@localhost
    ServerName wordpress.tp
    
    # Esta directiva es opcional
    ServerAlias www.wordpress.tp

    # Directorio raiz para la aplicacion
    DocumentRoot /var/www/wordpress.tp

    # Restringir el acceso a la subred 192.168.11.0/24
    <Directory "/var/www/wordpress.tp">
        Require ip 192.168.11.0/24
    </Directory>

    # Directorio protegido con contraseña
    <Directory "/var/www/wordpress.tp/protegido">
        AuthType Basic
        AuthName "Restricted Content"
        AuthUserFile /etc/apache2/htpasswd
        Require valid-user
    </Directory>

    ErrorLog ${APACHE_LOG_DIR}/error.log
    CustomLog ${APACHE_LOG_DIR}/access.log combined
</VirtualHost>
```



#### Creación de usuario y contraseña

Comando `htpasswd`.

```
# htpasswd -c /etc/apache2/htpasswd usuario
```



#### Creación de directorio raíz y directorio protegido

Se crea el `DocumentRoot`.

```
# mkdir /var/www/wordpress.tp
```

Se crea el directorio protegido.

```
# mkdir /var/www/wordpress.tp/protegido
```

#### Habilitacion del puerto 8080

El fichero `/etc/apache2/ports.conf` contiene la lista de puertos que Apache2 bloquea para uso exclusivo, aquí se añaden los nuevos puertos de escucha que podrán ser usados por los VirtualHosts.

Fichero `/etc/apache2/ports.conf`.

```
# añadir al archivo
Listen 8080
```



#### Activar sitio

Una vez terminadas la configuraciones creamos el enlace simbólico de `sites-available` a `sites-enabled`.

```
# a2ensite wordpress.tp.conf
```

Normalmente aplicamos la configuración con el comando `reload` de `systemctl`, pero en este caso al modificar los puertos de escucha debemos reiniciar el servicio.

```
# systemctl restart apache2
```



#### Prueba

Actualmente el dominio `wordpress.tp` apunta a la dirección IP `192.168.11.200`, ahora el VirtualHost ha sido creado y activado, por lo que debería de responder con el contenido del directorio raíz ya que este no tiene un fichero indice dentro.

Se usa el comando `curl` en un sistema operativo que tenga configurado como servidor dns primario el servidor `192.168.11.210`.

```
$ curl -v -s wordpress.tp:8080
*   Trying 192.168.11.200:8080...
* Connected to wordpress.tp (192.168.11.200) port 8080 (#0)
> GET / HTTP/1.1
> Host: wordpress.tp:8080
> User-Agent: curl/7.81.0
> Accept: */*
> 
* Mark bundle as not supporting multiuse
< HTTP/1.1 200 OK
< Date: Wed, 26 Jun 2024 18:52:44 GMT
< Server: Apache/2.4.59 (Debian)
< Vary: Accept-Encoding
< Content-Length: 556
< Content-Type: text/html;charset=UTF-8
< 
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2 Final//EN">
<html>
 <head>
  <title>Index of /</title>
 </head>
 <body>
<h1>Index of /</h1>
  <table>
   <tr><th valign="top"><img src="/icons/blank.gif" alt="[ICO]"></th><th><a href="?C=N;O=D">Name</a></th><th><a href="?C=M;O=A">Last modified</a></th><th><a href="?C=S;O=A">Size</a></th><th><a href="?C=D;O=A">Description</a></th></tr>
   <tr><th colspan="5"><hr></th></tr>
   <tr><th colspan="5"><hr></th></tr>
</table>
<address>Apache/2.4.59 (Debian) Server at wordpress.tp Port 8080</address>
</body></html>
```



### Etapa 4:  Despliegue de wordpress

#### Instalación de extensiones php

Librerías necesarias para el correcto funcionamiento de wordpress.

```
# apt install php-curl php-gd php-mbstring php-xml php-xmlrpc php-soap php-intl php-zip
```



#### Creación de base de datos

Se usara el usuario `wp_user` con la contraseña `1234` con privilegios sobre la base de datos `wp_db`.

> Claramente la contraseña es algo oculto, en este tipo de documentos se usa como ejemplo.



Acceder a la consola `mysql`.

```
# msyql
```

Base de datos.

```
> CREATE DATABASE wp_db;
```

Usuario.

```
> CREATE USER 'wp_user'@'%' IDENTIFIED BY '1234';
```

Asignación de privilegios.

```
> GRANT ALL ON wp_db.* TO 'wp_user'@'%';
```

Salr.

```
> quit
```



#### Obtención de fuentes

Dentro de un directorio, por ejemplo  `/tmp`, descargamos el paquete con la aplicación.

>  Esto es debido a que se dio así, la forma en la que realizamos estos pasos es a conveniencia y comodidad del administrador.



Acceder al directorio.

```
# cd /tmp
```

Descargar paquete.

```
# wget https://wordpress.org/latest.zip
```

Descomprimir.

```
# unzip latest.zip
```



> Es posible que algunos comandos no se encuentren, no hace falta indicar que se deben instalar ni explicar como se instalan, son herramientas triviales, no es el mismo caso que si es necesario indicar que se debe instalar para el stack lamp.



#### Configurar Wordpress

Se ha determinado que el usuario es `wp_user` con el password `1234`, y la base de datos es `wp_db`.

Copiar fichero de configuracion.

```
# cp wordpress/wp-config-sample.php wordpress/wp-config.php
```

Editar las directivas correspondientes. Fichero `/tmp/wordpress/wp-config.php`.

```
/** The name of the database for WordPress */
define( 'DB_NAME', 'wp_db' );

define( 'DB_USER', 'wp_user' );

/** Database password */
define( 'DB_PASSWORD', '1234' );
```

#### Mover aplicación

Una vez finalizada la configuración movemos los archivos al directorio raíz de la aplicación.

```
# mv /tmp/wordpress/* /var/www/wordpress.tp
```



#### Permisos y propietarios

Procesos de apache2.

```
# ps aux | grep apache2
www-data    9216  0.0  0.0   3408   168 ?        Ss   15:25   0:00 /usr/bin/htcacheclean -d 120 -p /var/cache/apache2/mod_cache_disk -l 300M -n
root        9523  0.0  1.1 204664 22268 ?        Ss   15:50   0:00 /usr/sbin/apache2 -k start
www-data    9524  0.0  0.6 205380 12908 ?        S    15:50   0:00 /usr/sbin/apache2 -k start
www-data    9525  0.0  0.6 205388 12540 ?        S    15:50   0:00 /usr/sbin/apache2 -k start
www-data    9526  0.0  0.6 205380 12908 ?        S    15:50   0:00 /usr/sbin/apache2 -k start
www-data    9527  0.0  0.5 205332 11260 ?        S    15:50   0:00 /usr/sbin/apache2 -k start
www-data    9528  0.0  0.5 205332 11260 ?        S    15:50   0:00 /usr/sbin/apache2 -k start

```

El usuario `www-data` es quien gestiona los procesos que atienden solicitudes, tambien podemos verificar esto en el archivo `apache2.conf`.

```
# cat /etc/apache2/apache2.conf | grep User
User ${APACHE_RUN_USER}
```

Fichero `envars`.

```
# cat /etc/apache2/envvars | grep USER
export APACHE_RUN_USER=www-data

# cat /etc/apache2/envvars | grep GROUP
export APACHE_RUN_GROUP=www-data
```



Aplicar dueño y grupo.

```
# chown www-data:www-data /var/www/wordpress.tp -R
```



Permisos de directorio de lectura y escritura para dueño, lectura para grupo y nada para otros.

```
# find /var/www/wordpress.tp -type d -exec chmod 750 {} \;
```

Verificación.

```
# ls -ld /var/www/wordpress.tp
ls -ld /var/www/wordpress.tp

# ls -l /var/www/wordpress.tp | grep ^d
drwxr-x---  2 www-data www-data  4096 jun 26 15:42 protegido
drwxr-x---  9 www-data www-data  4096 jun 24 14:16 wp-admin
drwxr-x---  4 www-data www-data  4096 jun 24 14:16 wp-content
drwxr-x--- 30 www-data www-data 12288 jun 24 14:16 wp-includes
```

Permisos de archivo de lectura y escritura para dueño, lectura para grupo y nada para otros.

```
# find /var/www/wordpress.tp -type f -exec chmod 640 {} \;
```

Verificación.

```
# ls -l /var/www/wordpress.tp | grep ^-
-rw-r-----  1 www-data www-data   405 feb  6  2020 index.php
-rw-r-----  1 www-data www-data 19915 dic 31 21:02 license.txt
-rw-r-----  1 www-data www-data  7401 dic  8  2023 readme.html
-rw-r-----  1 www-data www-data  7387 feb 13 11:19 wp-activate.php
-rw-r-----  1 www-data www-data   351 feb  6  2020 wp-blog-header.php
-rw-r-----  1 www-data www-data  2323 jun 14  2023 wp-comments-post.php
-rw-r-----  1 www-data www-data  2984 jun 26 16:11 wp-config.php
-rw-r-----  1 www-data www-data  3012 nov 22  2023 wp-config-sample.php
-rw-r-----  1 www-data www-data  5638 may 30  2023 wp-cron.php
-rw-r-----  1 www-data www-data  2502 nov 26  2022 wp-links-opml.php
-rw-r-----  1 www-data www-data  3927 jul 16  2023 wp-load.php
-rw-r-----  1 www-data www-data 50917 ene 16 14:31 wp-login.php
-rw-r-----  1 www-data www-data  8525 sep 16  2023 wp-mail.php
-rw-r-----  1 www-data www-data 28427 mar  2 07:47 wp-settings.php
-rw-r-----  1 www-data www-data 34385 jun 19  2023 wp-signup.php
-rw-r-----  1 www-data www-data  4885 jun 22  2023 wp-trackback.php
-rw-r-----  1 www-data www-data  3246 mar  2 10:49 xmlrpc.php
root@apache:/var/www# 
```



#### Completar instalación

En este punto ya podemos completar la instalación de la aplicación mediante el navegador:

[http://wordpress.tp:8080](http://wordpress.tp:8080)



## Conclusión



En este trabajo práctico, hemos configurado y desplegado un entorno LAMP para alojar la aplicación WordPress, siguiendo una serie de pasos estructurados y detallados. Empezamos por configurar un servidor DNS para gestionar el dominio `wordpress.tp`, permitiendo que nuestro servidor web Apache2 capture las peticiones dirigidas a este dominio.

Luego, instalamos y configuramos el stack LAMP (Linux, Apache, MariaDB, PHP) para preparar el entorno de ejecución necesario para WordPress. A continuación, definimos un VirtualHost en Apache que escucha en el puerto 8080, restringiendo el acceso a una subred específica y protegiendo con contraseña un directorio dentro del sitio web.

Finalmente, desplegamos WordPress, configuramos una base de datos, y aseguramos los permisos y propietarios adecuados para los archivos y directorios de la aplicación. Este proceso nos ha permitido no solo poner en funcionamiento un sitio web seguro y funcional, sino también entender mejor las interacciones y configuraciones necesarias para un entorno de servidor web eficiente y seguro.

Con este proyecto, se logra una comprensión práctica de cómo desplegar aplicaciones web complejas y se establecen buenas prácticas en la administración y seguridad de servidores.



## Fuentes

Aquí las fuentes.